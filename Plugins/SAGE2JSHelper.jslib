﻿mergeInto(LibraryManager.library, {

  GetCookieJS: function(name) {
	var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
	var name = Pointer_stringify(name);

    for(var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) {
			var returnStr = c.substring(name.length + 1, c.length);
			var bufferSize = lengthBytesUTF8(returnStr) + 1;
			var buffer = _malloc(bufferSize);
			stringToUTF8(returnStr, buffer, bufferSize);
            return buffer;
        }
    }
	return "";
  },
});