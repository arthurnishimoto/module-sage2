﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using UnityEngine;
using System.Runtime;
using System.Runtime.InteropServices;
using UnityEngine.Networking;

// Note: For UWP, make sure Capabilities are set:
// File/Build Settings/Player Settings/Publishing Settings/Capabilites
// InternetClientServer, PrivateNetworkClientServer, Microphone, SpatialMapping
// Also after build, double check that Capabilities are still enabled in Package.appxmanifest
public class WebSocket
{
	private Uri mUrl;
    bool m_IsConnected = false;

    public WebSocket(Uri url)
	{
		mUrl = url;

		string protocol = mUrl.Scheme;
		if (!protocol.Equals("ws") && !protocol.Equals("wss"))
			throw new ArgumentException("Unsupported protocol: " + protocol);
	}

	public string RecvString()
	{
#if !UNITY_WSA || UNITY_EDITOR
        byte[] retval = Recv();
        if (retval == null)
			return null;
		return Encoding.UTF8.GetString (retval);
#else
        return RecvStr();
#endif

    }

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
	private static extern int SocketCreate (string url);

	[DllImport("__Internal")]
	private static extern int SocketState (int socketInstance);

	[DllImport("__Internal")]
	private static extern void SocketSend (int socketInstance, byte[] ptr, int length);

	[DllImport("__Internal")]
	private static extern void SocketRecv (int socketInstance, byte[] ptr, int length);

	[DllImport("__Internal")]
	private static extern int SocketRecvLength (int socketInstance);

	[DllImport("__Internal")]
	private static extern void SocketClose (int socketInstance);

	[DllImport("__Internal")]
	private static extern int SocketError (int socketInstance, byte[] ptr, int length);

	int m_NativeRef = 0;

	public void Send(byte[] buffer)
	{
		SocketSend (m_NativeRef, buffer, buffer.Length);
	}

    public void Send(string srt)
    {
        byte[] buffer = Encoding.ASCII.GetBytes(srt);
        SocketSend(m_NativeRef, buffer, buffer.Length);
    }

    public byte[] Recv()
	{
		int length = SocketRecvLength (m_NativeRef);
		if (length == 0)
			return null;
		byte[] buffer = new byte[length];
		SocketRecv (m_NativeRef, buffer, length);
		return buffer;
	}

	public IEnumerator Connect()
	{
		m_NativeRef = SocketCreate (mUrl.ToString());
        Debug.Log("Websocket: Connect() " + mUrl.ToString());
		while (SocketState(m_NativeRef) == 0) {
            Debug.Log(SocketState(m_NativeRef));
            m_IsConnected = true;
			yield return 0;
        }
	}
 
	public void Close()
	{
		SocketClose(m_NativeRef);
	}

    public void ClearError()
	{
		
	}

	public string error
	{
		get {
			const int bufsize = 1024;
			byte[] buffer = new byte[bufsize];
			int result = SocketError (m_NativeRef, buffer, bufsize);

			if (result == 0)
				return null;

			return Encoding.UTF8.GetString (buffer);				
		}
	}

    public bool IsConnected()
    {
        return m_IsConnected;
    }
#elif !UNITY_WSA
    WebSocketSharp.WebSocket m_Socket;
	Queue<byte[]> m_Messages = new Queue<byte[]>();
	string m_Error = null;
    
	public IEnumerator Connect()
	{
		m_Socket = new WebSocketSharp.WebSocket(mUrl.ToString());
		m_Socket.OnMessage += (sender, e) => m_Messages.Enqueue (e.RawData);
		m_Socket.OnOpen += (sender, e) => m_IsConnected = true;
		m_Socket.OnError += (sender, e) => m_Error = e.Message;
		m_Socket.ConnectAsync();
		while (!m_IsConnected && m_Error == null)
			yield return 0;
	}

	public void Send(byte[] buffer)
	{
		m_Socket.Send(buffer);
	}

    public void Send(string jsonStr)
    {
        m_Socket.Send(jsonStr);
    }

    public byte[] Recv()
	{
		if (m_Messages.Count == 0)
			return null;
		return m_Messages.Dequeue();
	}

	public void Close()
	{
		m_Socket.Close();
	}

    public void ClearError()
    {
        m_Error = null;
    }

    public string error
	{
		get {
			return m_Error;
		}
    }

    public bool IsConnected()
    {
        return m_IsConnected;
    }

#else
    WebSocketUWP.WebSocketUWP m_Socket;
	Queue<string> m_Messages = new Queue<string>();
	string m_Error = null;
    
	public IEnumerator Connect()
	{
		m_Socket = new WebSocketUWP.WebSocketUWP(mUrl.ToString());
        m_Socket.OnMessage += (sender, e) => m_Messages.Enqueue(e.data);
        m_Socket.OnOpen += (sender, e) => m_IsConnected = true;;
        m_Socket.OnError += (sender, e) => m_Error = e.Message;
        m_Socket.ConnectAsync();
		while (!m_IsConnected && m_Error == null)
			yield return 0;
	}

	public void Send(byte[] buffer)
	{
		m_Socket.Send(buffer);
	}

    public void Send(string jsonStr)
    {
        m_Socket.Send(jsonStr);
    }

	public string RecvStr()
	{
		if (m_Messages.Count == 0)
			return null;
		return m_Messages.Dequeue();
	}

	public void Close()
	{
		m_Socket.Close();
	}

    public void ClearError()
    {
        m_Error = null;
    }

    public string error
	{
		get {
			return m_Error;
		}
    }

    public bool IsConnected()
    {
        return m_IsConnected;
    }
#endif
}