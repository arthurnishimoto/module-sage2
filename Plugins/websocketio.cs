﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.Networking;

/// <summary>Lightweight object around the 'Simple Web Sockets of Unity WebGL' asset.
/// http://u3d.as/gHp
/// </summary>
public class WebSocketIO : MonoBehaviour {

    /// <summary>websocket object handling the communication with the server
    /// </summary>
    WebSocket ws;
    
    [SerializeField]
    string url;

    /// <summary>list of messages to be handled (name + callback)
    /// </summary>
    Hashtable messages = new Hashtable();

    /// <summary>list of listeners on other side of connection
    /// </summary>
    Hashtable remoteListeners = new Hashtable();

    /// <summary>list of local listeners on this side of connection
    /// </summary>
    Hashtable localListeners = new Hashtable();

    string sessionHash;
    SAGE2.SAGE2ClientType clientType;

    [SerializeField]
    int aliasCount;

    public enum ConnectionState { NotConnected, Connecting, Connected }
    public ConnectionState connectionState = ConnectionState.NotConnected;

    public bool showDebug;
    public bool showIncomingMessages;

    [SerializeField]
    bool logMessages;

    ArrayList messageLog = new ArrayList();

    [SerializeField]
    bool dumpLogToFile;

    bool readyForOpen;

    public void Start()
    {
    }

    public void Init(string url = "", string session = "", SAGE2.SAGE2ClientType clientType = SAGE2.SAGE2ClientType.display)
    {
        sessionHash = session;
        this.clientType = clientType;

        remoteListeners["#WSIO#addListener"] = "0000";
        localListeners["0000"] = "#WSIO#addListener";
        aliasCount++;

        if (url.Length > 0)
        {
            this.url = url;
        }
        else
        {
            // If WebGL build, get the full application URL
            if (Application.absoluteURL.Length > 0)
            {
                string applicationURL = Application.absoluteURL;

                // Parse out the sage2 server url. Should be the first non-empty
                // string after http(s). Should also include :port number as well.
                string[] urlParsed = applicationURL.Split('/');
                bool https = urlParsed[0].Contains("https");
                foreach (string s in urlParsed)
                {
                    if (!s.Contains("http") && s.Length > 0)
                    {
                        // Server only, no port
                        this.url = https ? "wss" : "ws" + "://" + s;
                        break;
                    }
                }
            }
        }
        //StartCoroutine("Connect");
    }

    public void open(string callbackObject, string callbackFunction)
    {
        StartCoroutine("Connect", new string[] { callbackObject, callbackFunction} );
    }

    /// <summary>Creates and handles communication with server
    /// Socket is connected when addClient message is received
    /// from the SAGE2 server
    /// </summary>
    IEnumerator Connect(string[] callback)
    {
        connectionState = ConnectionState.Connecting;
        if (showDebug)
            Debug.Log("Connecting to: '" + url + "'");

        ws = new WebSocket(new System.Uri(url));
        yield return StartCoroutine(ws.Connect());

        if (ws.IsConnected())
        {
            GameObject.Find(callback[0]).SendMessage(callback[1]);
            connectionState = ConnectionState.Connected;
        }

        while (true)
        {
            string reply = ws.RecvString();

            if (reply != null)
            {
                if (showDebug || showIncomingMessages)
                    Debug.Log("RecvString '" + reply + "'");

                if (logMessages)
                {
                    messageLog.Add("Recv: " + reply);
                }

                var msg = SimpleJSON.JSON.Parse(reply);
                var d = msg["d"];

                if (d["listener"] != null)
                {
                    if (d["listener"].Value.Length > 0)
                    {
                        remoteListeners[d["listener"].Value] = d["alias"].Value;
                    }
                }
                else if(msg["f"].ToString().Length > 0)
                {
                    string name = (string)localListeners[msg["f"].Value];
                    string[] callbackStrs = (string[])messages[name];
                    GameObject callbackObj = GameObject.Find(callbackStrs[0]);
                    if(callbackObj)
                    {
                        callbackObj.SendMessage(callbackStrs[1], d.ToString());
                    }
                }
            }
            if (ws.error != null && connectionState != ConnectionState.NotConnected)
            {
                Debug.LogError("Error: " + ws.error);
                ws.ClearError();
                Close();
            }
            yield return 0;
        }
    }

    /// <summary>Send a message with a given name and payload (format> f:name d:payload)
    /// </summary>
    /// <param name="name">{String} name of the message (i.e. RPC)</param>
    /// <param name="data">{Object} data to be sent with the message</param>
    public bool Emit(string name, string data, int attempts = 16)
    {
        if (name.Length == 0)
        {
            Debug.LogError("Error: no message name specified");
            return false;
        }

        string alias = (string)remoteListeners[name];
        if (alias == null)
        {
            //Debug.LogWarning("Warning: not sending message, recipient has no listener (" + name + ")");
            StartCoroutine("EmitWait", new object[] { name, data, attempts });
            return false;
        }

        if (ws != null)
        {
            SAGE2.WsioMsg msgData;
            msgData.f = alias;
            msgData.d = JsonUtility.FromJson<SAGE2.d>(data);
            //byte[] msgByteArray = Encoding.ASCII.GetBytes(JsonUtility.ToJson(msgData));

            if (connectionState == ConnectionState.Connected)
            {
                ws.Send(JsonUtility.ToJson(msgData));

                if (logMessages)
                {
                    messageLog.Add("Send: " + JsonUtility.ToJson(msgData));
                }

                if (ws.error != null)
                {
                    Debug.LogWarning("Warning: " + ws.error);
                    ws.ClearError();
                    StartCoroutine("EmitWait", new object[] { name, data, attempts });
                }
                else
                {
                    if (showDebug)
                        Debug.Log("Emit: '" + JsonUtility.ToJson(data) + "'");
                    return true;
                }
            }
            else
            {
                StartCoroutine("EmitWait", new object[] { name, data, attempts });
            }
        }
        return false;
    }

    IEnumerator EmitWait(object[] param)
    {
        string name = (string)param[0];
        string data = (string)param[1];
        int attempts = (int)param[2];

        yield return new WaitForSeconds(4);

        if (attempts >= 0)
        {
            Emit(name, data, attempts - 1);
        }
        else
        {
            Debug.LogWarning("Warning: not sending message, recipient has no listener (" + name + ")");
        }
        yield return null;
    }

    public void On(string name, string callbackObject, string callbackFunction)
    {
        string alias = ("0000" + System.Convert.ToString(aliasCount, 16));
        localListeners[alias] = name;
        messages[name] = new string[] { callbackObject, callbackFunction };
        aliasCount++;
        SAGE2.WsioMsg message = new SAGE2.WsioMsg();
        message.d.listener = name;
        message.d.alias = alias;
        if (showDebug)
        {
            Debug.Log("ws.on added listener alias '" + alias + "' = '" + name + "'");
            Debug.Log(JsonUtility.ToJson(message));
        }
        Emit("#WSIO#addListener", JsonUtility.ToJson(message.d));
    }

    /// <summary>Deliberate close function
    /// </summary>
    void Close()
    {
        connectionState = ConnectionState.NotConnected;
        ws.Close();
    }

	// Update is called once per frame
	void Update () {
	    if(dumpLogToFile)
        {
            string[] lines = new string[messageLog.Count];
            messageLog.CopyTo(lines);
            System.IO.File.WriteAllLines(Application.dataPath + "/Resources/WebSocketIOLog.txt", lines);

            dumpLogToFile = false;
        }
	}
}
