// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// </summary>
public static class wsioJS
{
#if UNITY_WEBGL || UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void initJS(string url);

    [DllImport("__Internal")]
    private static extern void openJS(string callbackObject, string callbackFunction);

    [DllImport("__Internal")]
    private static extern void emitJS(string name, string data, int attempts);

    [DllImport("__Internal")]
    private static extern void onJS(string name, string callbackObject, string callbackFunction);
#endif
    private static WebSocketIO csWSIO;

    public static void WebsocketIO(GameObject parent = null, string url = null, bool debug = false)
    {
        //Debug.LogWarning("websocketio.cs calling initJS");
#if UNITY_WEBGL && !UNITY_EDITOR
        initJS(url);
#else
        if (parent != null)
        {
            csWSIO = parent.GetComponent<WebSocketIO>();
            if (csWSIO == null)
            {
                csWSIO = parent.AddComponent<WebSocketIO>();
            }
        }
        else
        {
            csWSIO = GameObject.Find("SAGE2Manager").GetComponent<WebSocketIO>();
            if (csWSIO == null)
            {
                csWSIO = GameObject.Find("SAGE2Manager").AddComponent<WebSocketIO>();
            }
        }


        csWSIO.Init(url);
        csWSIO.showDebug = debug;
        //Debug.LogWarning("websocketio:WebsocketIO() only works in WebGL builds");
#endif
    }

    public static void open(string callbackObject, string callbackFunction)
    {
        //Debug.LogWarning("websocketio.cs calling open");
#if UNITY_WEBGL && !UNITY_EDITOR
        openJS(callbackObject, callbackFunction);
#else
        csWSIO.open(callbackObject, callbackFunction);
        //Debug.LogWarning("websocketio:open() only works in WebGL builds");
#endif
    }

    public static void emit(string name, string data, int attempts = 16)
    {
        //Debug.LogWarning("websocketio.cs calling emit");
        //Debug.LogWarning("websocketio.cs emit name: " + name);
        //Debug.LogWarning("websocketio.cs emit data: " + data);
#if UNITY_WEBGL && !UNITY_EDITOR
        emitJS(name, data, attempts);
#else
        //Debug.LogWarning("websocketio:emit() only works in WebGL builds");
        csWSIO.Emit(name, data, attempts);
#endif
    }

    public static void on(string name, string callbackObject, string callbackFunction)
    {
        //Debug.LogWarning("websocketio.cs calling on");
#if UNITY_WEBGL && !UNITY_EDITOR
        onJS(name, callbackObject, callbackFunction);
#else
        //Debug.LogWarning("websocketio:on() only works in WebGL builds");
        //Debug.LogWarning("    websocketio:on() name: " + name);
        //Debug.LogWarning("    websocketio:on() callbackObject: " + callbackObject);
        //Debug.LogWarning("    websocketio:on() callbackFunction: " + callbackFunction);
        csWSIO.On(name, callbackObject, callbackFunction);
#endif
    }

}