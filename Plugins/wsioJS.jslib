// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018


var WebSocketIOJS = {
	
	initJS: function(urlPtr) {
		// console.log("jslib: initJS");
		url = Pointer_stringify(urlPtr);
  
		if (url !== undefined && url !== null) {
			this.url = url;
		} else {
			this.url = (window.location.protocol === "https:" ? "wss" : "ws") + "://" + window.location.host +
						"/" + window.location.pathname.split("/")[1];
		}

		/**
		 * websocket object handling the communication with the server
		 *
		 * @property ws
		 * @type WebSocket
		 */
		this.ws = null;

		/**
		 * list of messages to be handled (name + callback)
		 *
		 * @property messages
		 * @type Object
		 */
		this.messages = {};

		/**
		 * number of aliases created for listeners
		 *
		 * @property aliasCount
		 * @type Integer
		 */
		this.aliasCount = 1;

		/**
		 * list of listeners on other side of connection
		 *
		 * @property remoteListeners
		 * @type Object
		 */
		this.remoteListeners = {"#WSIO#addListener": "0000"};

		/**
		 * list of local listeners on this side of connection
		 *
		 * @property localListeners
		 * @type Object
		 */
		this.localListeners = {"0000": "#WSIO#addListener"};

		/**
		 * bytes sent property
		 * @property bytesWritten
		 * type       {Number}
		 */
		this._bytesWritten = 0;

		/**
		 * bytes received property
		 * @property bytesWritten
		 * type       {Number}
		 */
		this._bytesRead = 0;
		
		if(this._initialized !== true) {
			/**
			 * getter for the bytesRead property
			 */
			Object.defineProperty(this, "bytesRead", {
				get: function () {
					return this._bytesRead;
				}
			});
			
			
			/**
			 * getter for the bytesWritten property
			 */
			Object.defineProperty(this, "bytesWritten", {
				get: function () {
					return this._bytesWritten;
				}
			});
			
			this._initialized = true;
		}
	},
	
	emitJS: function(namePtr, dataPtr, attempts, _this) {
		// console.log("jslib: emitJS");
		var name;
		var data;
		
		if(typeof namePtr !== 'string')
			name = Pointer_stringify(namePtr);
		else
			name = namePtr;
		if(typeof dataPtr !== 'string')
			data = Pointer_stringify(dataPtr);
		else
			data = dataPtr;

		if (name === null || name === "") {
			console.log("jslib> emitJS - Error: no message name specified");
			return;
		}
		
		// Recursive calls will lose the root this reference
		// so we pass it though the calls as _this
		// If root, set _this
		if (_this === undefined)
		{
			_this = this;
		}

		var message;
		var alias = _this.remoteListeners[name];
		if (alias === undefined) {
			if (attempts === undefined) {
				attempts = 16;
			}
			if (attempts >= 0) {
				setTimeout(function() {
					_emitJS(name, data, attempts - 1, _this);
				}, 4);
			} else {
				console.log("jslib> emitJS - Warning: not sending message, recipient has no listener (" + name + ")");
			}
			return;
		}
		
		// Make sure websocket is ready to send
		if (_this.ws.readyState !== _this.ws.OPEN) {
			setTimeout(function() {
				_emitJS(name, data, attempts - 1, _this);
			}, 4);
			return;
		}
		
		// send binary data as array buffer
		if (data instanceof Uint8Array) {
			// build an array with the name of the function
			var funcName = new Uint8Array(4);
			funcName[0] = alias.charCodeAt(0);
			funcName[1] = alias.charCodeAt(1);
			funcName[2] = alias.charCodeAt(2);
			funcName[3] = alias.charCodeAt(3);
			message = new Uint8Array(4 + data.length);
			// copy the name of the function first
			message.set(funcName, 0);
			// then copy the payload
			message.set(data, 4);
			// send the message using websocket
			_this.ws.send(message.buffer);
			// update property
			_this._bytesWritten += message.buffer.byteLength;
		} else {
			// send data as JSON string
			message = JSON.stringify({f: alias, d: JSON.parse(data)});
			// console.log(message);
			_this.ws.send(message);
			// update property
			_this._bytesWritten += message.length;
		}
	},
	
	openJS__deps: ['emitJS'],
	openJS: function(callbackObject, callbackFunction) {		
		// console.log("jslib: openJS");
		
		var _this = this;

		// console.log('WebsocketIO> open', this.url);
		this.ws = new WebSocket(this.url);
		this.ws.binaryType = "arraybuffer";
		//this.ws.onopen = callback;
		SendMessage( Pointer_stringify(callbackObject), Pointer_stringify(callbackFunction) );
		
		// Handler when a message arrives
		this.ws.onmessage = function(message) {
			var fName;
			// text message
			if (typeof message.data === "string") {
				// update the bytes read value
				_this._bytesRead += message.data.length;
				// decode the message
				var msg = JSON.parse(message.data);
				fName = _this.localListeners[msg.f];
				if (fName === undefined) {
					console.log('jslib> openJS - No handler for message');
				}

				if (fName === "#WSIO#addListener") {
					_this.remoteListeners[msg.d.listener] = msg.d.alias;
					return;
				}
				//_this.messages[fName](msg.d);
				var callback = _this.messages[fName];
				SendMessage( callback.callbackObject, callback.callbackFunction, JSON.stringify(msg.d));
			} else {
				// update the bytes read value
				_this._bytesRead += message.data.byteLength;
				// decode the message
				var uInt8 = new Uint8Array(message.data);
				var func  = String.fromCharCode(uInt8[0]) +
							String.fromCharCode(uInt8[1]) +
							String.fromCharCode(uInt8[2]) +
							String.fromCharCode(uInt8[3]);
				fName = _this.localListeners[func];
				var buffer = uInt8.subarray(4, uInt8.length);
				//_this.messages[fName](buffer);
				var callback = _this.messages[fName];
				SendMessage( callback.callbackObject, callback.callbackFunction, String(buffer));
			}
		};
		// triggered by unexpected close event
		this.ws.onclose = function(evt) {
			console.log("jslib> openJS - socket closed");
			if ('close' in _this.messages) {
				_this.messages.close(evt);
			}
		};
	},
	
	/**
	* Set a message handler for a given name
	*
	* @method on
	* @param name {String} name for the handler
	* @param callback {Function} handler to be called for a given name
	*/
	onJS__deps: ['emitJS'],
	onJS: function(namePtr, callbackObject, callbackFunction) {
		var name = Pointer_stringify(namePtr);
		var callbackObj = Pointer_stringify(callbackObject);
		var callbackFcn = Pointer_stringify(callbackFunction);
		
		var alias = ("0000" + this.aliasCount.toString(16)).substr(-4);
		this.localListeners[alias] = name;
		this.messages[name] = {callbackObject: callbackObj, callbackFunction: callbackFcn};
		this.aliasCount++;
		if (name === "close") {
			return;
		}

		_emitJS('#WSIO#addListener', JSON.stringify({listener: name, alias: alias}), 16, this);
	},
};

mergeInto(LibraryManager.library, WebSocketIOJS);