﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SAGEPointer : MonoBehaviour
{

    [SerializeField]
    string address;

    [SerializeField]
    Vector3 pointerPosition; // in SAGE2 coords

    [Header("Appearance")]
    [SerializeField]
    string label;

    [SerializeField]
    Color color;

    [SerializeField]
    bool visible;

    [SerializeField]
    Image pointerImg;

    [SerializeField]
    Sprite pointerSprite;

    [SerializeField]
    Sprite pointerSpriteAlt;

    Sprite currentSprite;

    [SerializeField]
    Text labelText;

    [Header("Interaction")]
    [SerializeField]
    bool overApp;

    [SerializeField]
    SAGE2_app app;

    [SerializeField]
    Vector2 pointerPositionAppCoords;

    // Use this for initialization
    void Start()
    {
        pointerImg.sprite = pointerSprite;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreatePointer(string id, string label, string color, Vector3 position, bool visible = false)
    {
        this.address = id;
        this.label = label;

        Color newColor = Color.red;
        ColorUtility.TryParseHtmlString(color, out newColor);
        this.color = newColor;

        SetPosition(position);
        this.visible = visible;

        labelText.text = label;

        pointerImg.gameObject.SetActive(this.visible);
        pointerImg.color = this.color;

        gameObject.name = "Pointer(" + label + ")";

    }

    public void ShowPointer(string id, string label, string color, Vector3 position, bool visible = false)
    {
        this.address = id;
        this.label = label;

        Color newColor = Color.red;
        ColorUtility.TryParseHtmlString(color, out newColor);
        this.color = newColor;

        SetPosition(position);
        this.visible = true;

        labelText.text = label;
        pointerImg.color = this.color;

        if (GetComponentInParent<SAGE2_UI>().IsRenderingUI())
        {
            pointerImg.gameObject.SetActive(this.visible);
        }
        else
        {
            pointerImg.gameObject.SetActive(false);
        }

        gameObject.name = "Pointer(" + label + ")";
    }

    public void HidePointer()
    {
        visible = false;
        pointerImg.gameObject.SetActive(this.visible);
    }

    public void SetVisible(bool value)
    {
        visible = value;
        if (GetComponentInParent<SAGE2_UI>().IsRenderingUI())
        {
            pointerImg.gameObject.SetActive(this.visible);
        }
        else
        {
            pointerImg.gameObject.SetActive(false);
        }
    }

    public void UpdatePointerPosition(string id, string label, string color, Vector3 position, bool visible = false)
    {
        this.address = id;
        this.label = label;

        Color newColor = Color.red;
        ColorUtility.TryParseHtmlString(color, out newColor);
        this.color = newColor;

        SetPosition(position);
        this.visible = true;

        labelText.text = label;
        pointerImg.color = this.color;

        if (GetComponentInParent<SAGE2_UI>().IsRenderingUI())
        {
            pointerImg.gameObject.SetActive(this.visible);
        }
        else
        {
            pointerImg.gameObject.SetActive(false);
        }
        
        gameObject.name = "Pointer(" + label + ")";
    }

    void SetPosition(Vector3 position)
    {
        pointerPosition = position;

        Vector3 adjustedPosition = Vector3.zero;

        Vector2 wallResolution = GetComponentInParent<SAGE2_UI>().GetWallResolution();

        adjustedPosition.x = position.x;
        adjustedPosition.y = wallResolution.y - position.y;

        adjustedPosition.x -= wallResolution.x / 2.0f;
        adjustedPosition.y -= wallResolution.y / 2.0f;

        transform.localPosition = adjustedPosition;
    }

    public string GetLabel()
    {
        return label;
    }

    public void UseDefaultSprite()
    {
        pointerImg.sprite = pointerSprite;
    }

    public void UseAltSprite()
    {
        if (pointerSpriteAlt != null)
            pointerImg.sprite = pointerSpriteAlt;
    }

    // Unity adjusted position
    public Vector3 GetPosition()
    {
        return transform.localPosition;
    }

    // SAGE2 server position
    public Vector3 GetRawPosition()
    {
        return pointerPosition;
    }

    public string GetAddress()
    {
        return address;
    }

    public bool IsVisible()
    {
        return visible;
    }

    public bool IsOverApp(SAGE2_app app)
    {
        Vector3 pointerPos = GetPosition();
        bool hasPointerOver = false; // Pointer is over app (ignoring layers)
        if (pointerPos.x >= app.GetPosition().x - app.GetSize().x / 2.0f &&
                    pointerPos.x <= app.GetPosition().x + app.GetSize().x / 2.0f &&
                    pointerPos.y >= app.GetPosition().y - app.GetSize().y / 2.0f &&
                    pointerPos.y <= app.GetPosition().y + app.GetSize().y / 2.0f
                    )
        {
            hasPointerOver = true;
        }
        else
        {
            hasPointerOver = false;
        }


        if (hasPointerOver)
        {
            // Check if new app is on top
            if (this.app == null || app.GetSortOrder() > this.app.GetSortOrder())
            {
                this.app = app;
                overApp = true;
                
            }
            UseAltSprite();

            Vector2 wallResolution = GetComponentInParent<SAGE2_UI>().GetWallResolution();

            pointerPositionAppCoords.x = GetRawPosition().x - app.GetRawPosition().x;

            // Correct for Unity apps origin at lower left corner (verses SAGE2 upper left)
            pointerPositionAppCoords.y = -((wallResolution.y - GetRawPosition().y) - (wallResolution.y - app.GetRawPosition().y));

        }
        else if (app == this.app)
        {
            this.app = null;
            overApp = false;
            pointerPositionAppCoords = new Vector2(-1, -1);
            UseDefaultSprite();
        }

        return overApp;
    }

    public string GetOverAppInfo()
    {
        if (overApp)
        {
            return "OverApp '" + app.GetTitle() + " " + pointerPositionAppCoords;
        }
        else
        {
            return "";
        }
    }
}
