﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SAGE2ServerConnectUI : MonoBehaviour {

    [SerializeField]
    SAGE2Manager sage2Manager;

    [SerializeField]
    InputField serverInput;

    [SerializeField]
    Button connectToServerButton;

    [SerializeField]
    GameObject connectPanel;


    Text connectButtonText;
	// Use this for initialization
	void Start () {
		if(sage2Manager)
        {
            if(serverInput)
            {
                serverInput.text = sage2Manager.GetServerIP();
                serverInput.onValueChanged.AddListener(sage2Manager.SetSAGE2ServerIP);
            }
            if (connectToServerButton)
            {
                connectToServerButton.onClick.AddListener(sage2Manager.ConnectToServer);
                connectButtonText = connectToServerButton.GetComponentInChildren<Text>();
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (sage2Manager)
        {
            connectButtonText.text = sage2Manager.IsConnectedToServer() ? "Connected" : "Connect";
        }

    }

    public void ToggleConnectPanel(bool value)
    {
        connectPanel.SetActive(!connectPanel.activeSelf);
    }
}
