﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

/// <summary>Unity implementation of SAGE2_DisplayUI
/// </summary>
public class SAGE2_UI : MonoBehaviour {

    SAGE2.Delegate createAppWindowPositionSizeOnly;
    SAGE2.Delegate setItemPosition;
    SAGE2.Delegate setItemPositionAndSize;

    SAGE2.DelegateTexture setBackgroundImage;

    SAGE2.Delegate createAppWindow;
    SAGE2.Delegate updateItemOrder;
    SAGE2.Delegate deleteElement;

    [SerializeField]
    Image background;

    [SerializeField]
    GameObject appPrefab;

    [Header("SAGE2 Wall Info")]
    [SerializeField]
    Vector2 totalResolution;

    [SerializeField]
    Vector2 layout;

    [SerializeField]
    float titleBarHeight;

    protected Hashtable appList = new Hashtable();

    //sstring hostURL;

    [SerializeField]
    protected Text infoText;

    protected bool render = true;
    bool displayConfigured = false;
    // Use this for initialization
    void Start () {
        InitUI();
    }

    public bool IsRenderingUI()
    {
        return render;
    }

    public Vector2 GetWallResolution()
    {
        return totalResolution;
    }

    protected void InitUI()
    {
        // Global
        setItemPosition += wsSetItemPosition;
        setItemPositionAndSize += wsSetItemPositionAndSize;

        setBackgroundImage += cbSetBackgroundImage;

        SAGE2.On("setItemPosition", setItemPosition);
        SAGE2.On("setItemPositionAndSize", setItemPositionAndSize);

        // SAGE2 Display client
        if (SAGE2.GetSAGE2Manager().GetClientType() == SAGE2.SAGE2ClientType.display)
        {
            createAppWindow += wsCreateAppWindow;
            updateItemOrder += wsUpdateItemOrder;
            deleteElement += wsDeleteElement;

            SAGE2.On("createAppWindow", createAppWindow);
            SAGE2.On("updateItemOrder", updateItemOrder);
            SAGE2.On("deleteElement", deleteElement);
        }
        // SAGE2 UI
        else if (SAGE2.GetSAGE2Manager().GetClientType() == SAGE2.SAGE2ClientType.sageUI)
        {
            createAppWindowPositionSizeOnly += wsCreateAppWindowPositionSizeOnly;

            SAGE2.On("createAppWindowPositionSizeOnly", createAppWindowPositionSizeOnly);
        }

        if (!render && background)
        {
            background.gameObject.SetActive(false);
        }

        StartCoroutine("SetupDisplay");
    }

    IEnumerator SetupDisplay()
    {
        JSONNode data = SAGE2.GetConfigFile();

        while(data == null)
        {
            data = SAGE2.GetConfigFile();
            yield return null;
        }
 

        totalResolution = new Vector2(data["resolution"]["width"].AsInt * Mathf.Max(data["layout"]["columns"].AsInt, 1), data["resolution"]["height"].AsInt * Mathf.Max(data["layout"]["rows"].AsInt, 1));

        RectTransform rect = GetComponent<RectTransform>();
        if (rect)
            rect.sizeDelta = new Vector2(totalResolution.x, totalResolution.y);

        if (SAGE2.GetSAGE2Manager().GetClientType() == SAGE2.SAGE2ClientType.display)
        {
            if (background)
                background.color = SAGE2.ColorFromHtmlString(data["background"]["color"]);

            if (render && data["background"]["image"]["url"].IsString)
            {
                StartCoroutine(SAGE2.LoadImageFromSAGE2(new object[] {"/" + data["background"]["image"]["url"], setBackgroundImage }));
            }
        }
        else if (SAGE2.GetSAGE2Manager().GetClientType() == SAGE2.SAGE2ClientType.sageUI)
        {
            if (background)
                background.color = SAGE2.ColorFromHtmlString("#B3B3B3");
        }

        float minDim = Mathf.Min(totalResolution.x, totalResolution.y);
        float maxDim = Mathf.Max(totalResolution.x, totalResolution.y);

        float xRatio = Screen.width / totalResolution.x;
        float yRatio = Screen.height / totalResolution.y;

        if (xRatio < yRatio)
            transform.localScale = new Vector3(xRatio, xRatio, 1);
        else
            transform.localScale = new Vector3(yRatio, yRatio, 1);

        if (background)
            background.GetComponent<RectTransform>().sizeDelta = new Vector2(totalResolution.x, totalResolution.y);

        titleBarHeight = Mathf.Round(0.025f * minDim);
        //transform.localPosition = new Vector3(-Screen.width / 2.0f, -Screen.height / 2.0f, 0);

        displayConfigured = true;
        yield return null;
    }

    void wsCreateAppWindowPositionSizeOnly(string dataString)
    {
        // Create UI client app
        JSONNode data = JSONNode.Parse(dataString);

        GameObject appObj = Instantiate(appPrefab, transform) as GameObject;
        appObj.SetActive(true);
        SAGE2_app s2app = appObj.GetComponent<SAGE2_app>();
        if(render)
        {
            s2app.InitRenderApp();
        }
        s2app.SetAppID(data["id"]);
        //s2app.SetTitle(data["id"]);
        s2app.SetTitle(data["title"]);
        s2app.SetSize(data["width"].AsInt, data["height"].AsInt, titleBarHeight);
        s2app.SetPosition(data["left"], data["top"] + titleBarHeight);

        if (data["application"] == "media_stream")
        {
            // Shared Screen
            s2app.SetSharedScreen(data["title"], data["color"]);
        }
        else if (data["icon"].IsString)
        {
            s2app.SetIcon(data["icon"] + "_512.jpg");
        }

        appList[data["id"].Value] = s2app;
    }

    void wsSetItemPosition(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        SAGE2_app app = (SAGE2_app)appList[data["elemId"]];
        if(app != null)
        {
            app.SetPosition(data["elemLeft"], data["elemTop"]);
        }
    }

    void wsSetItemPositionAndSize(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        SAGE2_app app = (SAGE2_app)appList[data["elemId"]];
        if (app != null)
        {
            app.SetSize(data["elemWidth"].AsInt, data["elemHeight"].AsInt, titleBarHeight);
            app.SetPosition(data["elemLeft"].AsInt, data["elemTop"].AsInt + titleBarHeight);
        }
    }

    void wsCreateAppWindow(string dataString)
    {
        // Create Display client app
        JSONNode data = JSONNode.Parse(dataString);

        GameObject appObj = Instantiate(appPrefab, transform) as GameObject;
        SAGE2_app s2app = appObj.GetComponent<SAGE2_app>();
        if (render)
        {
            s2app.InitRenderApp();
        }
        s2app.SetAppID(data["id"]);
        s2app.SetTitle(data["title"]);
        s2app.SetSize(data["width"].AsInt, data["height"].AsInt, titleBarHeight);
        s2app.SetPosition(data["left"].AsInt, data["top"].AsInt + titleBarHeight);

        if (data["application"] == "image_viewer")
        {
            s2app.SetIcon(data["data"]["img_url"]);
        }
        else if (data["icon"].IsString)
        {
            s2app.SetIcon(data["icon"] + "_512.jpg");
        }

        appList[data["id"].Value] = s2app;
    }

    void wsUpdateItemOrder(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        foreach (string app_id in appList.Keys)
        {
            SAGE2_app s2app = (SAGE2_app)appList[app_id];
            if( data[app_id] != null )
            {
                s2app.SetOrder(data[app_id].AsInt);
            }
        }
    }

    void wsDeleteElement(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        SAGE2_app s2app = (SAGE2_app)appList[data["elemId"].Value];
        if (s2app != null)
        {
            Destroy(s2app.gameObject);
        }
        appList.Remove(data["elemId"].Value);
    }

    void cbSetBackgroundImage(Texture2D texture)
    {
        background.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        background.color = Color.white;
    }

    public bool IsDisplayConfigured()
    {
        return displayConfigured;
    }

    // Update is called once per frame
    void Update () {
        if (!render)
            return;

        if (totalResolution.x > 0 && totalResolution.y > 0)
        {
            float xRatio = transform.parent.GetComponent<RectTransform>().rect.width / totalResolution.x;
            float yRatio = transform.parent.GetComponent<RectTransform>().rect.height / totalResolution.y;

            if (xRatio < yRatio)
                transform.localScale = new Vector3(xRatio, xRatio, 1);
            else
                transform.localScale = new Vector3(yRatio, yRatio, 1);

            background.GetComponent<RectTransform>().sizeDelta = new Vector2(totalResolution.x, totalResolution.y);
        }

        if (infoText)
        {
            infoText.text = "App Resolution: " + Screen.width + " " + Screen.height + "\n";
            foreach(DictionaryEntry entry in appList)
            {
                SAGE2_app app = (SAGE2_app)entry.Value;
                infoText.text += "  App '" + app.GetAppID() + "' Size: " + app.GetSize() + "\n";
            }
        }
	}
}
