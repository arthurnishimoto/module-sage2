﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

/// <summary>Observer version SAGE2_DisplayUI for using wall data without rendering
/// </summary>
public class SAGE2_UI_Observer : SAGE2_UI
{
    // Use this for initialization
    void Start () {
        render = false;

        InitUI();
    }

    private void Update()
    {
        SAGE2_app[] apps = GetComponentsInChildren<SAGE2_app>();
        SAGE2Manager s2manager = GetComponent<SAGE2Manager>();

        infoText.text = "SAGE2 UI Observer";
        infoText.text += s2manager.IsConnectedToServer() ? "\nConnected" : "\nNot connected to server";
        infoText.text += "\nInput MousePosition: " + Input.mousePosition;
        infoText.text += "\nRunning SAGE2 Apps:";
        for(int i = 0; i < apps.Length; i++)
        {
            infoText.text += "\n   [" + i + "]: " +  apps[i].GetAppID() + " '" + apps[i].GetTitle() + "' " + apps[i].GetRawPosition() + " " + apps[i].GetSize();
        }

        SAGEPointer[] pointers = GetComponentsInChildren<SAGEPointer>();
        infoText.text += "\nSAGE2 Pointers:";
        for (int i = 0; i < pointers.Length; i++)
        {
            infoText.text += "\n   [" + i + "]: '" + pointers[i].GetLabel() + "' " + pointers[i].GetRawPosition() + " " + pointers[i].IsVisible() + " " + pointers[i].GetOverAppInfo();
        }
    }
}
