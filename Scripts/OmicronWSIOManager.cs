﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using omicronConnector;

public class OmicronWSIOManager : MonoBehaviour {

    [SerializeField]
    bool connectToServer;

    [SerializeField]
    string serverAddress;

    [SerializeField]
    string server;

    [SerializeField]
    string serverWS;

    bool usingHTTPS;

    [SerializeField]
    bool showWSDebug;

    [SerializeField]
    bool connected;

    [Header("Requested Service Types")]
    [SerializeField]
    bool enableMocap;

    [SerializeField]
    bool enableAudio;

    // Setup delegates for listener callback functions
    public delegate void Delegate(string data);

    Delegate onMocapEvent;
    Delegate onAudioEvent;

    [Serializable]
    public struct OmicronEvent
    {
        public string sourceIP;
        public uint timestamp;
        public int sourceId;
        public int serviceId;
        public int serviceType;
        public int type;
        public int flags;
        public float posx;
        public float posy;
        public float posz;
        public float orw;
        public float orx;
        public float ory;
        public float orz;
    }

    // Use this for initialization
    void Start () {
        if(connectToServer)
        {
            ConnectToServer();
        }

        onMocapEvent += wsOnMocapEvent;
        onAudioEvent += wsOnAudioEvent;

        if (enableMocap)
        {
            On("onMocapEvent", onMocapEvent);
        }
        if (enableAudio)
        {
            On("onAudioEvent", onAudioEvent);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ConnectToServer()
    {
        connectToServer = true;

        // If WebGL build, get the full application URL
        if (Application.absoluteURL.Length > 0)
        {
            string applicationURL = Application.absoluteURL;

            // Parse out the sage2 server url. Should be the first non-empty
            // string after http(s). Should also include :port number as well.
            string[] urlParsed = applicationURL.Split('/');
            usingHTTPS = urlParsed[0].Contains("https");

            foreach (string s in urlParsed)
            {
                if (!s.Contains("http") && s.Length > 0)
                {
                    serverWS = (usingHTTPS ? "wss" : "ws") + "://" + serverAddress;
                    server = (usingHTTPS ? "https" : "http") + "://" + serverAddress;
                    break;
                }
            }
        }
        else
        {
            serverWS = "ws" + "://" + serverAddress;
            server = "http" + "://" + serverAddress;
        }

        Debug.Log("Connecting to '" + serverWS + "'");

        wsioJS.WebsocketIO(gameObject, serverWS, showWSDebug);
        wsioJS.open(gameObject.name, "wsOpen");
    }

    void wsOpen()
    {
        //Debug.Log("SAGE2Manager:wsOpen() called");
        connected = true;
    }

    public static void On(string name, Delegate callback)
    {
        if (callback != null)
        {
            // callback.target = 'Object Name (Script)'
            string target = callback.Target.ToString();
            string callbackObject = target.Substring(0, callback.Target.ToString().LastIndexOf('(')).Trim();

            // callback.method = 'void Function(params)'
            string callbackFunction = callback.Method.ToString().Split(' ')[1].ToString().Split('(')[0].Trim();

            wsioJS.on(name, callbackObject, callbackFunction);
        }
    }

    public void wsOnMocapEvent(string dataString)
    {
        //Debug.Log("wsOnMocapEvent() called");
        //Debug.Log(dataString);
        OmicronEvent e = JsonUtility.FromJson<OmicronEvent>(dataString);
        //Debug.Log(e.sourceId + ": " + e.posx + " " + e.posy + " " + e.posz);
    }

    public void wsOnAudioEvent(string dataString)
    {
        //Debug.Log("wsOnAudioEvent() called");
        //Debug.Log(dataString);
        OmicronEvent e = JsonUtility.FromJson<OmicronEvent>(dataString);
        //Debug.Log(e.sourceId + ": " + e.posx + " " + e.posy + " " + e.posz);
        
        if(GetComponent<OmicronManager>())
        {
            GetComponent<OmicronManager>().AddEvent(OmicronEventToEventData(e));
        }
    }

    public EventData OmicronEventToEventData(OmicronEvent e)
    {
        EventData evt = new EventData();
        evt.timestamp = e.timestamp;
        evt.sourceId = (uint)e.sourceId;
        evt.serviceId = (int)e.serviceId;
        evt.serviceType = (omicron.EventBase.ServiceType)e.serviceType;
        evt.type = (uint)e.type;
        evt.flags = (uint)e.flags;
        evt.posx = e.posx;
        evt.posy = e.posy;
        evt.posz = e.posz;
        evt.orw = e.orw;
        evt.orx = e.orx;
        evt.ory = e.ory;
        evt.orz = e.orz;
        return evt;
    }
}
