﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SAGE2_app : MonoBehaviour {

    RectTransform rect;

    [SerializeField]
    string appID;

    [SerializeField]
    Text titleText;

    [SerializeField]
    Vector2 position;

    [SerializeField]
    Vector2 size;

    [SerializeField]
    Image icon;

    [SerializeField]
    Image background;

    [SerializeField]
    RectTransform titleBar;

    SAGE2.DelegateTexture setIconImage;

    [SerializeField]
    GameObject sharedScreenIcon;

    bool renderApp = false;

    // Use this for initialization
    void Start () {
        rect = GetComponent<RectTransform>();

        // Match parent scale
        transform.localScale = Vector3.one;

        if(!renderApp)
        {
            background.gameObject.SetActive(false);
            icon.gameObject.SetActive(false);
            titleBar.gameObject.SetActive(false);
        }
    }
	
    public void InitRenderApp()
    {
        background.gameObject.SetActive(true);
        icon.gameObject.SetActive(true);
        titleBar.gameObject.SetActive(true);

        renderApp = true;
    }

	// Update is called once per frame
	void Update () {
        Hashtable pointers = SAGE2.GetPointers();
        foreach(DictionaryEntry entry in pointers)
        {
            SAGEPointer pointer = (SAGEPointer)entry.Value;
            if(pointer.IsOverApp(this))
            {
                //Debug.Log("Pointer: " + pointer.GetPosition());
                //Debug.Log("Pointer: " + pointer.GetLabel());
                //Debug.Log("Over App: " + appID);
            }
        }
    }

    public void SetAppID(string id)
    {
        appID = id;
        gameObject.name = "SAGE2_app_" + appID;
    }

    public string GetAppID()
    {
        return appID;
    }

    public void SetPosition(float left, float top)
    {
        StartCoroutine("SetPositionCR", new Vector2(left, top));
    }

    IEnumerator SetPositionCR(Vector2 position)
    {
        // Offset position (SAGE2 origin: upper left, Unity origin: center) and invert y
        Vector3 offsetPosition = Vector3.zero;

        while(GetComponentInParent<SAGE2_UI>().IsDisplayConfigured() == false)
        {
            yield return null;
        }

        Vector2 wallResolution = GetComponentInParent<SAGE2_UI>().GetWallResolution();

        offsetPosition.x = position.x + size.x / 2.0f;
        offsetPosition.y = wallResolution.y - position.y - size.y / 2.0f;

        offsetPosition.x -= wallResolution.x / 2.0f;
        offsetPosition.y -= wallResolution.y / 2.0f;

        rect.localPosition = offsetPosition;

        rect.localEulerAngles = Vector3.zero;

        yield return null;
    }

    // Unity adjusted position
    public Vector3 GetPosition()
    {
        return rect.localPosition;
    }

    // SAGE2 server position
    public Vector3 GetRawPosition()
    {
        return position;
    }

    public void SetSize(float width, float height, float titleBarHeight)
    {
        rect = GetComponent<RectTransform>();
        rect.sizeDelta = new Vector3(width, height);
        size = new Vector3(width, height);

        titleBar.localPosition = Vector3.up * (titleBarHeight / 2.0f + height / 2.0f);
        titleBar.sizeDelta = new Vector3(0, titleBarHeight);
    }

    public Vector2 GetSize()
    {
        return size;
    }

    public string GetTitle()
    {
        return titleText.text;

    }
    public void SetTitle(string title)
    {
        titleText.text = title;
    }

    public void SetIcon(string url)
    {
        setIconImage = cbSetIconImage;
        if (renderApp)
        {
            StartCoroutine(SAGE2.LoadImageFromSAGE2(new object[] { url, setIconImage }));
        }
    }

    public void SetOrder(int order)
    {
        GetComponent<Canvas>().sortingOrder = order;
    }

    public int GetSortOrder()
    {
        return GetComponent<Canvas>().sortingOrder;
    }

    public void SetSharedScreen(string label, string color)
    {
        if (renderApp)
        {
            GameObject icon = Instantiate(sharedScreenIcon, transform) as GameObject;
            icon.GetComponentInChildren<Image>().color = SAGE2.ColorFromHtmlString(color);
            icon.GetComponentInChildren<Text>().text = label;
            icon.GetComponent<RectTransform>().sizeDelta = rect.sizeDelta;
            icon.GetComponent<RectTransform>().localPosition = Vector3.zero;
        }
    }

    void cbSetIconImage(Texture2D texture)
    {
        if (texture != null)
        {
            icon.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
        icon.color = Color.white;
        icon.preserveAspect = true;
    }
}
