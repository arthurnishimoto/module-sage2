﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

public class SAGEPointerManager : MonoBehaviour {

    [SerializeField]
    Transform mainCanvas;

    [SerializeField]
    GameObject sagePointerPrefab;

    Hashtable sagePointers = new Hashtable();

    SAGE2.Delegate createSagePointer;
    SAGE2.Delegate showSagePointer;
    SAGE2.Delegate hideSagePointer;
    SAGE2.Delegate updateSagePointerPosition;
    SAGE2.Delegate changeSagePointerMode;

    SAGEPointer mousePointer;

    // Use this for initialization
    void Start () {
        SAGE2.SetSAGEPointerManager(this);

        // Setup delegates for callback function
        createSagePointer += wsCreateSagePointer;
        showSagePointer += wsShowSagePointer;
        hideSagePointer += wsHideSagePointer;
        updateSagePointerPosition += wsUpdateSagePointerPosition;
        changeSagePointerMode += wsChangeSagePointerMode;

        // Register function with SAGE2 server
        SAGE2.On("createSagePointer", createSagePointer);
        SAGE2.On("showSagePointer", showSagePointer);
        SAGE2.On("hideSagePointer", hideSagePointer);
        SAGE2.On("updateSagePointerPosition", updateSagePointerPosition);
        SAGE2.On("changeSagePointerMode", changeSagePointerMode);
    }

    public Hashtable GetSAGEPointers()
    {
        return sagePointers;
    }

    void wsCreateSagePointer(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        GameObject pointerObj = Instantiate(sagePointerPrefab, mainCanvas.transform) as GameObject;
        SAGEPointer newPointer = pointerObj.GetComponent<SAGEPointer>();

        sagePointers[data["id"].Value] = newPointer;

        Vector3 pos = new Vector3(data["left"].AsInt, data["top"].AsInt);
        newPointer.CreatePointer(data["id"].Value, data["label"].Value, data["color"].Value, pos, data["visible"].AsBool);
    }

    void wsShowSagePointer(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        SAGEPointer pointer = (SAGEPointer)sagePointers[data["id"].Value];
        if (pointer)
        {
            Vector3 pos = new Vector3(data["left"].AsInt, data["top"].AsInt);
            pointer.ShowPointer(data["id"].Value, data["label"].Value, data["color"].Value, pos, data["visible"].AsBool);
        }
    }

    void wsHideSagePointer(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        SAGEPointer pointer = (SAGEPointer)sagePointers[data["id"].Value];
        if (pointer)
        {
            pointer.HidePointer();
        }
    }

    void wsUpdateSagePointerPosition(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);
        SAGEPointer pointer = (SAGEPointer)sagePointers[data["id"].Value];
        if (pointer)
        {
            Vector3 pos = new Vector3(data["left"].AsInt, data["top"].AsInt);
            pointer.UpdatePointerPosition(data["id"].Value, data["label"].Value, data["color"].Value, pos, data["visible"].AsBool);
        }
    }

    void wsChangeSagePointerMode(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        //Debug.Log("wsChangeSagePointerMode");
        //Debug.Log(data);
    }

    // Update is called once per frame
    void Update () {

    }
    
}
