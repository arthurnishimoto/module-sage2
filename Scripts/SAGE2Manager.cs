﻿// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;
using UnityEngine.Networking;
using SimpleJSON;

/// <summary>Main entry point for the SAGE2-Unity API
/// </summary>
public class SAGE2 : MonoBehaviour
{
    static SAGE2Manager sage2Manager_instance;
    static SAGEPointerManager sage2PointerManager_instance;

    public enum PointerType { Pointer, Touch };
    public enum SAGE2ClientType { display, sageUI };

    /// <summary>Generic struct for incoming SAGE2 JSON string messages
    /// </summary>
    [Serializable]
    public struct WsioMsg
    {
        public string f;
        public d d;
    }

    [Serializable]
    public struct d
    {
        // Listeners
        public string listener;
        public string alias;

        // addClient
        public string clientType;
        public Requests requests;

        public string clientID;
        public string browser;
        public string session;
    }

    [Serializable]
    public struct Requests
    {
        public bool config;
        public bool version;
        public bool time;
        public bool console;
    }

    // Setup delegates for listener callback functions
    public delegate void Delegate(string data);

    public delegate void DelegateTexture(Texture2D texture);

    public delegate void DelegateObject(object[] obj);

    /// <summary>SAGE pointer object
    /// </summary>
    public struct SAGEPointer
    {
        int id;
        string label;
        Color color;
        Vector2 position;
        PointerType sourceType;

        /// <summary>Creates a new SAGE pointer
        /// </summary>
        /// <param name="id">Identifier of the new pointer</param>
        /// <param name="label">Name of the user</param>
        /// <param name="color">Pointer color</param>
        /// <param name="position">Position of pointer</param>
        /// <param name="sourceType">Type of pointer</param>
        public SAGEPointer(int id, string label, Color color, Vector2 position, PointerType sourceType = PointerType.Pointer)
        {
            this.id = id;
            this.label = label;
            this.color = color;
            this.sourceType = sourceType;
            this.position = position;
        }
    };

    public static void On(string name, Delegate callback)
    {
        if (sage2Manager_instance.connectToSAGE2)
        {
            if (callback != null)
            {
                // callback.target = 'Object Name (Script)'
                string target = callback.Target.ToString();
                string callbackObject = target.Substring(0, callback.Target.ToString().LastIndexOf('(')).Trim();

                // callback.method = 'void Function(params)'
                string callbackFunction = callback.Method.ToString().Split(' ')[1].ToString().Split('(')[0].Trim();

                wsioJS.on(name, callbackObject, callbackFunction);
            }
        }
        else
        {
            
            sage2Manager_instance.AddOnCallToQueue(name, callback);
        }
    }

    public static IEnumerator LoadImageFromWeb(object[] param)
    {
        string url = (string)param[0];

        // Convert to a proper URL from a local path
        url = url.Replace('\\', '/');
        System.Uri uriAddress = new System.Uri(url);

        SAGE2.DelegateTexture callback = (SAGE2.DelegateTexture)param[1];

        // Start a download of the given URL
        using (WWW www = new WWW(uriAddress.AbsoluteUri))
        {
            // Wait for download to complete
            yield return www;

            // Return texture
            callback(www.texture);
        }
    }

    /**
     * Special function to get a texture from the connected SAGE2 server.
     * Proper SAGE2 host URL is automatically prepended to url
     */
    public static IEnumerator LoadImageFromSAGE2(object[] param)
    {
        while (SAGE2.GetHostURL().Length == 0)
            yield return null;

        string url = SAGE2.GetHostURL() + (string)param[0];

        // Convert to a proper URL from a local path
        url = url.Replace('\\', '/');
        System.Uri uriAddress = new System.Uri(url);

        SAGE2.DelegateTexture callback = (SAGE2.DelegateTexture)param[1];

        // Start a download of the given URL
        using (WWW www = new WWW(uriAddress.AbsoluteUri))
        {
            // Wait for download to complete
            yield return www;

            // Return texture
            callback(www.texture);
        }
    }

    public static Color ColorFromHtmlString(string hexString)
    {
        Color newColor = Color.white;
        ColorUtility.TryParseHtmlString(hexString, out newColor);
        return newColor;
    }

    public static void SetSAGE2Manager(SAGE2Manager manager)
    {
        sage2Manager_instance = manager;
    }

    public static void SetSAGEPointerManager(SAGEPointerManager manager)
    {
        sage2PointerManager_instance = manager;
    }

    public static SAGE2Manager GetSAGE2Manager()
    {
        if(sage2Manager_instance)
        {
            return sage2Manager_instance;
        }
        return GameObject.Find("SAGE2Manager").GetComponent<SAGE2Manager>();
    }

    public static Hashtable GetPointers()
    {
        if (sage2PointerManager_instance)
        {
            return sage2PointerManager_instance.GetSAGEPointers();
        }
        SAGEPointerManager pointerManager = GameObject.Find("SAGE2Manager").GetComponent<SAGEPointerManager>();
        if (pointerManager)
        {
            return pointerManager.GetSAGEPointers();
        }
        return null;
    }

    public static bool UsingHTTPS()
    {
        return sage2Manager_instance.UsingHTTPS();
    }

    public static JSONNode GetConfigFile()
    {
        return sage2Manager_instance.GetConfigFile();
    }

    public static string GetHostURL()
    {
        return sage2Manager_instance.GetHostURL();
    }
}

/// <summary>SAGE2Manager handles the API calls between SAGE2 and Unity
/// </summary>
public class SAGE2Manager : MonoBehaviour {
    public static SAGE2Manager instance;

    [SerializeField]
    public bool connectToSAGE2;

    [SerializeField]
    string sage2ServerAddress = "localhost";

    string sage2Server;
    string sage2ServerWS;

    [SerializeField]
    bool connected;

    [SerializeField]
    string session;

    [SerializeField]
    bool saveSessionHashToPrefs;

    [SerializeField]
    bool clearSessionHashFromPrefs;

    [SerializeField]
    SAGE2.SAGE2ClientType clientType = SAGE2.SAGE2ClientType.sageUI;

    // Server port connected to used as unique identifier
    // Shown on server console when client connects 'clientIP:port (clientType)'
    [SerializeField]
    int serverPort;

    [SerializeField]
    string hostURL;

    [SerializeField]
    Text connectionText;

    SAGE2.Delegate createSagePointer;
    public SAGE2.Delegate changeSagePointerMode;
    public SAGE2.Delegate updateSagePointerPosition;

    public SAGE2.Delegate eventInItem;

    public SAGE2.Delegate setupDisplayConfiguration;

    public bool showWSDebug;

    bool usingHTTPS;

    JSONNode config;

    struct OnCall
    {
        public string name;
        public SAGE2.Delegate callback;
    }

    ArrayList onCallQueue = new ArrayList();

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        SAGE2.SetSAGE2Manager(this);

        if (clearSessionHashFromPrefs)
        {
            PlayerPrefs.DeleteKey("session");
            PlayerPrefs.DeleteKey("serverIP");
            Debug.Log("Removed session to PlayerPrefs");
        }

        // Get the session cookie (WebGL build only)
        if (session.Length == 0)
            session = SAGE2JSHelper.GetCookie("session");
        
        if (session.Length == 0) // else try player prefs if non WebGL
            session = PlayerPrefs.GetString("session");
        
        if(PlayerPrefs.GetString("serverIP").Length > 0 )
        {
            sage2ServerAddress = PlayerPrefs.GetString("serverIP");
        }

        if (saveSessionHashToPrefs)
        {
            PlayerPrefs.SetString("session", session);
            PlayerPrefs.SetString("serverIP", sage2ServerAddress);
            Debug.Log("Saved session to PlayerPrefs");
        }

        connected = false;

        if (connectToSAGE2)
        {
            ConnectToServer();
        }
    }

    public SAGE2.SAGE2ClientType GetClientType()
    {
        return clientType;
    }

    public void wsCreateSagePointer(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);
        string pointerID = data["id"].Value;
        if (pointerID.IndexOf(':') >= 0)
        {
            string IP = pointerID.Substring(0, pointerID.IndexOf(':'));
            string portStr = pointerID.Substring(pointerID.IndexOf(':') + 1, pointerID.IndexOf('_') - (pointerID.IndexOf(':') + 1));
            int port = 0;
            int.TryParse(portStr, out port);

            if (serverPort == 0)
            {
                serverPort = port;
            }
        }
    }

    void wsOpen()
    {
        //Debug.Log("SAGE2Manager:wsOpen() called");
        if(connectionText)
            connectionText.text = "Connected to '" + sage2Server;
        connected = true;
        SAGE2.WsioMsg request = new SAGE2.WsioMsg();
        switch (clientType)
        {
            case (SAGE2.SAGE2ClientType.sageUI):
                request.d.clientType = "sageUI"; break;
            default:
                request.d.clientType = "display"; break;
        }
        request.d.clientID = "0";
        request.d.session = session;
        request.d.requests.config = true;
        request.d.requests.version = true;
        request.d.requests.time = true;
        request.d.requests.console = false;

        wsioJS.emit("addClient", JsonUtility.ToJson(request.d));

        foreach(OnCall queuedOnMessage in onCallQueue)
        {
            SAGE2.On(queuedOnMessage.name, queuedOnMessage.callback);
        }
    }

    void wsEventInItem(JSONNode data)
    {
        string app_id = data["id"];
        string eType = data["type"];
        Vector2 position = new Vector2(data["position"]["x"].AsInt, data["position"]["y"].AsInt);
        string user_id = data["user"]["id"];
        string user_label = data["user"]["label"];
        string user_color = data["user"]["color"];
        long date = data["date"].AsInt;
        Debug.Log("wsEventInItem appId: " + app_id);
        Debug.Log("   type: " + eType);
        Debug.Log("   position: " + position);
        Debug.Log("   user_id: " + user_id);
        Debug.Log("   user_label: " + user_label + " color: " + user_color);
    }

    void wsSetupDisplayConfiguration(string dataString)
    {
        JSONNode data = JSONNode.Parse(dataString);

        config = data;

        hostURL = (SAGE2.UsingHTTPS() ? "https" : "http") + "://" + config["host"] + ":" + (SAGE2.UsingHTTPS() ? config["secure_port"].AsInt : config["port"].AsInt);
    }

    // Update is called once per frame
    void Update ()
    {

    }

    public void SetSAGE2ServerIP(string address)
    {
        sage2ServerAddress = address;
    }

    public void ConnectToServer()
    {
        connectToSAGE2 = true;

        // If WebGL build, get the full application URL
        if (Application.absoluteURL.Length > 0)
        {
            string applicationURL = Application.absoluteURL;

            // Parse out the sage2 server url. Should be the first non-empty
            // string after http(s). Should also include :port number as well.
            string[] urlParsed = applicationURL.Split('/');
            usingHTTPS = urlParsed[0].Contains("https");

            foreach (string s in urlParsed)
            {
                if (!s.Contains("http") && s.Length > 0)
                {
                    sage2ServerWS = (usingHTTPS ? "wss" : "ws") + "://" + sage2ServerAddress;
                    sage2Server = (usingHTTPS ? "https" : "http") + "://" + sage2ServerAddress;
                    break;
                }
            }
        }
        else
        {
            sage2ServerWS = "ws" + "://" + sage2ServerAddress;
            sage2Server = "http" + "://" + sage2ServerAddress;
        }

        if (connectionText)
            connectionText.text = "Connecting to '" + sage2Server + "'";

        Debug.Log("Connecting to '" + sage2ServerWS + "'");

        wsioJS.WebsocketIO(gameObject, sage2ServerWS, showWSDebug);
        wsioJS.open(gameObject.name, "wsOpen");

        createSagePointer += wsCreateSagePointer;
        SAGE2.On("createSagePointer", createSagePointer);

        setupDisplayConfiguration += wsSetupDisplayConfiguration;
        SAGE2.On("setupDisplayConfiguration", setupDisplayConfiguration);
    }

    public bool IsConnectedToServer()
    {
        return connected;
    }

    public string GetServerIP()
    {
        return sage2ServerAddress;
    }

    public void AddOnCallToQueue(string name, SAGE2.Delegate callback)
    {
        OnCall onCallObject;
        onCallObject.name = name;
        onCallObject.callback = callback;

        onCallQueue.Add(onCallObject);
    }

    public bool UsingHTTPS()
    {
        return usingHTTPS;
    }

    public JSONNode GetConfigFile()
    {
        return config;
    }

    public string GetHostURL()
    {
        return hostURL;
    }
}
